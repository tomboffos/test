// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'flows/cubit/todo_cubit.dart' as _i5;
import 'flows/repository/todo_repository.dart' as _i3;
import 'flows/repository/todo_repository_impl.dart'
    as _i4; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.factory<_i3.TodoRepository>(() => _i4.TodoRepositoryImpl());
  gh.factory<_i5.TodoCubit>(() => _i5.TodoCubit(get<_i3.TodoRepository>()));
  return get;
}
