import 'package:flutter/material.dart';
import 'package:test/flows/presentation/create.dart';
import 'package:test/flows/presentation/index.dart';
import 'package:test/flows/presentation/start.dart';
import 'package:test/services/transition.dart';

class AppRouter {
  Route generateRouter(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case '/':
        return CustomPageRoute(const StartIndex());
      case '/index':
        return CustomPageRoute(const TodoIndex());
      case '/create':
        return CustomPageRoute(const CreateTodo());
      default:
        return MaterialPageRoute(builder: (context) => Container());
    }
  }
}
