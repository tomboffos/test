import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:test/flows/cubit/todo_cubit.dart';
import 'package:test/injection.dart';
import 'package:test/internal/router.dart';

class MyApp extends StatelessWidget {
  final AppRouter appRouter;
  const MyApp({Key? key, required this.appRouter}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, device, deviceType) {
      return BlocProvider(
        create: (context) => getIt.get<TodoCubit>(),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          onGenerateRoute: appRouter.generateRouter,
        ),
      );
    });
  }
}
