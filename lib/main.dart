import 'package:flutter/material.dart';
import 'package:test/injection.dart';
import 'package:test/internal/router.dart';

import 'internal/application.dart';

void main() {
  configureDependencies();
  runApp(MyApp(
    appRouter: AppRouter(),
  ));
}
