import 'package:flutter/material.dart';

class Status {
  String name;
  MaterialColor color;

  Status.fromJson(Map json)
      : name = json['status'],
        color = getColor(json['status']);

  static MaterialColor getColor(String status) {
    switch (status) {
      case 'completed':
        return Colors.green;
      case 'pending':
        return Colors.yellow;
      default:
        return Colors.red;
    }
  }
}
