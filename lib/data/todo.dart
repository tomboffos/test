import 'package:test/data/status.dart';

class Todo {
  int id;
  int userId;
  String title;
  DateTime dueOn;
  Status status;

  Todo.fromJson(Map json)
      : id = json['id'],
        userId = json['user_id'],
        title = json['title'],
        dueOn = DateTime.parse(json['due_on']),
        status = Status.fromJson(json);

  toJson() {
    return {
      'id': id,
      'user_id': userId,
      'title': title,
      'due_on': dueOn.toString(),
      'status': status.name
    };
  }
}
