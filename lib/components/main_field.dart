import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:test/constant.dart';

class MainTextField extends StatelessWidget {
  final String? text;
  final String? label;
  final onTap;
  final TextEditingController? controller;
  const MainTextField(
      {Key? key, this.text, this.label, this.controller, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 3.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label ?? '',
            style: AppConstant.todoStyle,
          ),
          SizedBox(
            height: 1.h,
          ),
          TextField(
              onTap: onTap ?? () => {},
              controller: controller,
              decoration: InputDecoration(
                  fillColor: Colors.grey[100],
                  filled: true,
                  hintText: text ?? '',
                  border: InputBorder.none)),
        ],
      ),
    );
  }
}
