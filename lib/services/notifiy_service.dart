import 'package:flutter/material.dart';

class NotifyService {
  static ScaffoldFeatureController showSuccessMessage(
      String text, BuildContext context) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(text),
      backgroundColor: Colors.greenAccent,
    ));
  }

  static ScaffoldFeatureController showErrorMessage(
      String text, BuildContext context) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        text,
        style: const TextStyle(color: Colors.white),
      ),
      backgroundColor: Colors.red,
    ));
  }
}
