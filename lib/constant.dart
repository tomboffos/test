import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AppConstant {
  static const apiUrl = 'https://gorest.co.in/public/v2';

  static const TextStyle appBarStyle =
      TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black);

  static const TextStyle todoStyle =
      TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black);

  static const TextStyle statusStyle =
      TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.grey);

  static const TextStyle buttonTextStyle =
      TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white);
}
