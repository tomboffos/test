import 'package:flutter/material.dart';
import 'package:test/data/todo.dart';

abstract class TodoRepository {
  Future<List<Todo>>? fetchTodos() {}

  Future<Todo?> saveTodo(
      Map<String, String> form, BuildContext context) async {}

  Future saveTodosModels(todoModels) async {}
}
