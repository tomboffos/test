import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test/constant.dart';
import 'package:test/data/todo.dart';
import 'package:test/flows/repository/todo_repository.dart';
import 'package:test/services/notifiy_service.dart';

@Injectable(as: TodoRepository)
class TodoRepositoryImpl implements TodoRepository {
  @override
  Future<List<Todo>>? fetchTodos() async {
    final response = await get(Uri.parse('${AppConstant.apiUrl}/todos'));

    List<Todo> todos = jsonDecode(response.body)
        .map((e) => Todo.fromJson(e))
        .toList()
        .cast<Todo>();

    String? localTodos =
        (await SharedPreferences.getInstance()).getString('todos');

    if (localTodos != null) {
      todos.addAll(jsonDecode(localTodos)
          .map((e) => Todo.fromJson(e))
          .toList()
          .cast<Todo>());
    }

    return todos;
  }

  @override
  Future<Todo?> saveTodo(Map<String, String> form, BuildContext context) async {
    final todos = (await SharedPreferences.getInstance()).getString('todos');

    if (form['title'] == null) {
      NotifyService.showErrorMessage('Type a title', context);
      return null;
    }

    if (form['due_on'] == null) {
      NotifyService.showErrorMessage('Type a dead line', context);
      return null;
    }

    Todo todo = Todo.fromJson({
      'status': 'pending',
      'id': 11,
      'title': form['title'],
      'user_id': 11,
      'due_on': form['due_on'],
    });

    if (todos != null) {
      List<Todo> todoModels = (jsonDecode(todos))
          .map((e) => Todo.fromJson(e))
          .toList()
          .cast<Todo>();

      todoModels.add(todo);

      saveTodosModels(todoModels);
    }
    saveTodosModels([todo]);

    NotifyService.showSuccessMessage('Created task successfully', context);

    return todo;
  }

  @override
  saveTodosModels(todoModels) async {
    (await SharedPreferences.getInstance()).setString(
        'todos', jsonEncode(todoModels.map((e) => e.toJson()).toList()));
  }
}
