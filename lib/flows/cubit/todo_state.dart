part of 'todo_cubit.dart';

@immutable
abstract class TodoState {}

class TodoInitial extends TodoState {}

class TodoLoaded extends TodoState {
  final List<Todo> todos;
  final List<Status> statuses;

  TodoLoaded(this.todos, this.statuses);
}
