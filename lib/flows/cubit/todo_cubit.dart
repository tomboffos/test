import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:test/data/status.dart';
import 'package:test/data/todo.dart';
import 'package:test/flows/repository/todo_repository.dart';

part 'todo_state.dart';

@injectable
class TodoCubit extends Cubit<TodoState> {
  final TodoRepository todoRepository;
  TodoCubit(this.todoRepository) : super(TodoInitial());
  List<Todo> allTodos = [];
  getTodos() async {
    final todos = await todoRepository.fetchTodos();

    if (todos != null) {
      allTodos = todos;

      List<Status> uniqueStatuses = todos
          .map((e) => e.status.name)
          .toSet()
          .toList()
          .map((e) => Status.fromJson({'status': e}))
          .toList()
          .cast<Status>();

      uniqueStatuses.insert(0, Status.fromJson({'status': 'All'}));

      emit(TodoLoaded(todos, uniqueStatuses));
    }
  }

  getByStatus(Status status) {
    List<Todo> filteredTodos = allTodos
        .expand((e) => [if (e.status.name == status.name) e])
        .toList()
        .cast<Todo>();

    if (state is TodoLoaded) {
      emit(TodoLoaded(status.name == 'All' ? allTodos : filteredTodos,
          (state as TodoLoaded).statuses));
    }
  }

  getByName(String name) {
    List<Todo> filteredTodos = allTodos
        .expand(
            (e) => [if (e.title.toLowerCase().contains(name.toLowerCase())) e])
        .toList()
        .cast<Todo>();
    if (state is TodoLoaded) {
      emit(TodoLoaded(filteredTodos, (state as TodoLoaded).statuses));
    }
  }

  Future saveTodo(Map<String, String> form, BuildContext context) async {
    Todo? todo = await todoRepository.saveTodo(form, context);

    if (todo != null) {
      if (state is TodoLoaded) {
        List<Todo> todos = (state as TodoLoaded).todos;
        todos.add(todo);

        emit(TodoLoaded(todos, (state as TodoLoaded).statuses));
      }
    }
  }
}
