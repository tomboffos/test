import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:test/constant.dart';
import 'package:test/flows/cubit/todo_cubit.dart';

class TodoIndex extends StatefulWidget {
  const TodoIndex({Key? key}) : super(key: key);

  @override
  State<TodoIndex> createState() => _TodoIndexState();
}

class _TodoIndexState extends State<TodoIndex> {
  bool searchMode = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<TodoCubit>(context).getTodos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1.0,
        title: searchMode
            ? TextField(
                onChanged: (value) =>
                    BlocProvider.of<TodoCubit>(context).getByName(value),
                decoration: const InputDecoration(
                    hintText: 'Type a task', border: InputBorder.none),
              )
            : const Text(
                'Board',
                style: AppConstant.appBarStyle,
              ),
        centerTitle: false,
        actions: [
          IconButton(
              onPressed: () {
                setState(() {
                  searchMode = !searchMode;
                });
              },
              icon: const Icon(
                Icons.search,
                color: Colors.black,
              ))
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 10.sp),
          child: BlocBuilder<TodoCubit, TodoState>(
            builder: (context, state) {
              if (state is! TodoLoaded) {
                return const Center(child: CircularProgressIndicator());
              }
              return Column(
                children: [
                  Container(
                    height: 5.h,
                    margin: EdgeInsets.only(bottom: 2.h),
                    decoration: const BoxDecoration(
                        border: Border(bottom: BorderSide(color: Colors.grey))),
                    child: ListView.builder(
                      itemBuilder: (context, index) => GestureDetector(
                        onTap: () => BlocProvider.of<TodoCubit>(context)
                            .getByStatus(state.statuses[index]),
                        child: Container(
                          margin: EdgeInsets.only(right: 25.sp),
                          child: Text(
                            state.statuses[index].name,
                            style: AppConstant.statusStyle,
                          ),
                        ),
                      ),
                      scrollDirection: Axis.horizontal,
                      itemCount: state.statuses.length,
                    ),
                  ),
                  ListView.builder(
                    itemBuilder: (context, index) => Container(
                      margin: EdgeInsets.only(bottom: 10.sp),
                      child: Row(
                        children: [
                          Container(
                            width: 40.sp,
                            height: 40.sp,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.sp),
                                border: Border.all(
                                    color: state.todos[index].status.color,
                                    width: 2)),
                          ),
                          SizedBox(
                            width: 20.sp,
                          ),
                          SizedBox(
                            width: 60.w,
                            child: Text(
                              state.todos[index].title,
                              style: AppConstant.todoStyle,
                            ),
                          )
                        ],
                      ),
                    ),
                    itemCount: state.todos.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                  ),
                ],
              );
            },
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FlatButton(
        onPressed: () => Navigator.pushNamed(context, '/create'),
        minWidth: 50.w,
        child: const Text(
          'Add a task',
          style: AppConstant.buttonTextStyle,
        ),
        color: Colors.green,
      ),
    );
  }
}
