import 'package:flutter/material.dart';

class StartIndex extends StatefulWidget {
  const StartIndex({Key? key}) : super(key: key);

  @override
  State<StartIndex> createState() => _StartIndexState();
}

class _StartIndexState extends State<StartIndex> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(const Duration(seconds: 1), () {
      Navigator.pushNamed(context, '/index');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset('logo.png'),
      ),
    );
  }
}
