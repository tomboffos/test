import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test/components/main_field.dart';
import 'package:test/constant.dart';
import 'package:sizer/sizer.dart';
import 'package:test/flows/cubit/todo_cubit.dart';

class CreateTodo extends StatefulWidget {
  const CreateTodo({Key? key}) : super(key: key);

  @override
  State<CreateTodo> createState() => _CreateTodoState();
}

class _CreateTodoState extends State<CreateTodo> {
  TextEditingController title = TextEditingController();
  TextEditingController dueTo = TextEditingController();

  bool loading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1.0,
        title: const Text(
          'Add a task',
          style: AppConstant.appBarStyle,
        ),
        centerTitle: false,
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: Container(
        padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 10.sp),
        child: Column(
          children: [
            MainTextField(
              controller: title,
              text: 'Design team meeting',
              label: 'Title',
            ),
            MainTextField(
              onTap: () => showCupertinoModalPopup(
                  context: context,
                  builder: (newContext) => Container(
                        height: MediaQuery.of(context).copyWith().size.height *
                            0.25,
                        color: Colors.white,
                        child: CupertinoDatePicker(
                          mode: CupertinoDatePickerMode.dateAndTime,
                          onDateTimeChanged: (value) {
                            setState(() {
                              dueTo.text = value.toString();
                            });
                          },
                          initialDateTime: DateTime.now(),
                          minimumYear: 2019,
                          maximumYear: 2051,
                        ),
                      )),
              controller: dueTo,
              text: '2021-02-20',
              label: 'Deadline',
            )
          ],
        ),
      )),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FlatButton(
        onPressed: () {
          if (!loading) {
            setState(() {
              loading = true;
            });
            BlocProvider.of<TodoCubit>(context).saveTodo(
                {'title': title.text, 'due_on': dueTo.text},
                context).then((value) => setState(() => loading = false));
          }
        },
        minWidth: 50.w,
        child: const Text(
          'Create task',
          style: AppConstant.buttonTextStyle,
        ),
        color: Colors.green,
      ),
    );
  }
}
